xskat (4.0-9) unstable; urgency=medium

  [ Debian Janitor ]
  * [a8ea653] Remove constraints unnecessary since buster (oldstable)
    * Build-Depends: Drop dependency on essential package dpkg-dev (>=
      1.16.1~).
    Changes-By: deb-scrub-obsolete

  [ Florian Ernst ]
  * [72119fb] d/patches/xskat-c99.patch: import from Fedora.
    Thanks to Florian Weimer <fweimer@redhat.com>, Jochen Sprickerhof
    <jspricke@debian.org>
    (Closes: #1075683)
  * [b5a80af] d/rules: set PREPROCESSMANPAGES= to avoid cpp-14 error.
    We don't install the processed pages anyways but stick to the provided
    ones.
  * [b92e648] d/copyright: update for 2024
  * [be59cd8] d/control: Recommends on xfonts-base (LP: #1818215)
  * [41566f0] d/salsa-ci.yml: added

 -- Florian Ernst <florian@debian.org>  Thu, 25 Jul 2024 16:29:07 +0200

xskat (4.0-8) unstable; urgency=medium

  * [cfb4e4a] allow cross-building.
    Thanks to Nguyen Hoang Tung (Closes: #930849)
  * [4786ca0] debian/control: update VCS links
  * [2757446] bump debhelper from deprecated 9 to debhelper-compat (= 13) in B-D
  * [bdd6f37] debian/watch: upgrade to recommended version
  * [343b3ce] debian/copyright: update year, drop space from License field,
    version Format field
  * [be12ca4] remove the obsolete menu file and its XPM file from the package

 -- Florian Ernst <florian@debian.org>  Sun, 09 Jan 2022 09:42:30 +0100

xskat (4.0-7) unstable; urgency=medium

  * [de2d700] debian/rules: enable all hardening

 -- Florian Ernst <florian@debian.org>  Wed, 01 Oct 2014 22:10:23 +0200

xskat (4.0-6) unstable; urgency=medium

  * [940a3e1] debian/menu: add icon reference (Closes: #738082)
  * [82f707e] debian/rules: import hardening buildflags
  * [5b20d87] debian/control: update Vcs-* and Standards-Version

 -- Florian Ernst <florian@debian.org>  Mon, 28 Apr 2014 16:10:28 +0200

xskat (4.0-5) unstable; urgency=low

  * [5ac0374] revert Makefile change from previous diff.gz
  * [ecb3ff5] redo packaging, using 3.0 (quilt) format, dh9 rules and dep5
    copyright
  * [fbfb16f] drop README files from deb as they don't provide any further
    info
  * [ec60b52] debian/changelog: s/Explicitely/Explicitly/ as suggested by
    lintian
  * [5127c7b] add xskat.desktop and xskat.xpm (converted from icon.xbm)
    (Closes: #527159)

 -- Florian Ernst <florian@debian.org>  Thu, 14 Jun 2012 11:51:07 +0200

xskat (4.0-4) unstable; urgency=low

  * rules: Explicitly set LDFLAGS to prevent FTBFS with dpkg-dev >= 1.14.17
    (Closes: #475976)
  * control: move Homepage to its own field
  * copyright: extended to actually list the copyright
  * Standards-Version 3.7.3

 -- Florian Ernst <florian@debian.org>  Mon, 14 Apr 2008 17:30:39 +0200

xskat (4.0-3) unstable; urgency=low

  * New maintainer (Closes: #355232)
  * Some minor packaging adjustments

 -- Florian Ernst <florian@debian.org>  Sat,  4 Mar 2006 16:18:12 +0100

xskat (4.0-2) unstable; urgency=low

  * Build-depend on libx11-dev instead of the obsolete catch-all
    xlibs-dev (closes: #346865)
  * Bump Standards-Version (nothing to do).
  * Debhelper compatibility level 5

 -- Falk Hueffner <falk@debian.org>  Wed, 11 Jan 2006 16:21:01 +0100

xskat (4.0-1) unstable; urgency=low

  * New upstream release.
  * Fix some lintian warnings.

 -- Falk Hueffner <falk@debian.org>  Tue, 25 May 2004 21:05:13 +0200

xskat (3.4-2) unstable; urgency=low

  * Update to policy 3.5.9.0:
    - Handle DEB_BUILD_OPTIONS noopt.
    - Add Build-Depends (closes: #190628).

 -- Falk Hueffner <falk@debian.org>  Sat, 26 Apr 2003 00:45:43 +0200

xskat (3.4-1) unstable; urgency=low

  * New upstream release.
  * Added menu entry for IRC play.

 -- Falk Hueffner <falk@debian.org>  Mon, 30 Oct 2000 21:24:15 +0100

xskat (3.3-1) unstable; urgency=low

  * New upstream version, honours $LANG (closes #44488).
  * Updated debian/copyright with clarified license.
  * Added menu hints.
  * New maintainer email address with GPG key.

 -- Falk Hueffner <falk@debian.org>  Thu, 30 Mar 2000 19:41:00 +0200

xskat (3.2-1) unstable; urgency=low

  * Use debhelper for the build process.
  * FHS.
  * Tweak description.
  * Tweak CFLAGS.
  * New upstream version.

 -- Falk Hueffner <falk.hueffner@student.uni-tuebingen.de>  Fri,  1 Oct 1999 12:54:55 +0200

xskat (3.0-1) unstable; urgency=low

  * New upstream version
    - Playing via Internet Relay Chat supported
    - Computer can suggest a card to play
    - Shortcut possible in all games
    - 3D look

 -- Falk Hueffner <falk.hueffner@student.uni-tuebingen.de>  Sat,  5 Dec 1998 09:47:52 +0100

xskat (2.3.1-1) unstable; urgency=low

  * new maintainer
  * new menu file
  * relocate binary to /usr/games
  * default language now english
  * new upstream version

 -- Falk Hueffner <falk.hueffner@student.uni-tuebingen.de>  Tue, 15 Sep 1998 21:33:13 +0200

xskat (2.2-1) unstable; urgency=low

  * remove mess introduced by someone "fixing" the changelog issue.
  * new debstd #14463
  * New upstream release #17978

 -- Christoph Lameter <clameter@debian.org>  Thu,  5 Mar 1998 08:00:11 -0800

xskat (2.0-1) unstable; urgency=low

  * libc6
  * New upstream release

 -- Christoph Lameter <clameter@debian.org>  Mon, 29 Sep 1997 20:02:16 -0700

xskat (1.7-1) unstable; urgency=low

  * new maintainer Helmut Geyer
  * new upstream version

 -- Helmut Geyer <Helmut.Geyer@iwr.uni-heidelberg.de>  Thu,  5 Jun 1997 20:29:03 +0200

xskat (1.6-2) unstable frozen; urgency=low

  * Change menu location to Games/Card #8430

 -- Christoph Lameter <clameter@debian.org>  Mon, 31 Mar 1997 09:52:30 -0800

xskat (1.6-1) unstable; urgency=low

  * Initial Release.

 -- Christoph Lameter <clameter@debian.org>  Fri, 21 Feb 1997 22:08:48 -0800
